# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-03 01:37+0000\n"
"PO-Revision-Date: 2023-03-26 14:27+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#: ui/main.qml:33
#, kde-format
msgid "Configure Hotspot"
msgstr ""

#: ui/main.qml:77
#, kde-format
msgid "Hotspot"
msgstr "Hotspot"

#: ui/main.qml:78
#, kde-format
msgid "Share your internet connection with other devices as a Wi-Fi network."
msgstr ""

#: ui/main.qml:93
#, kde-format
msgid "Settings"
msgstr ""

#: ui/main.qml:103
#, kde-format
msgid "Hotspot SSID"
msgstr "Hotspot SSID"

#: ui/main.qml:112
#, kde-format
msgid "Hotspot Password"
msgstr "Hotspot Password"

#~ msgid "Whether the wireless hotspot is enabled."
#~ msgstr "Whether the wireless hotspot is enabled."

#~ msgid "Enabled:"
#~ msgstr "Enabled:"

#~ msgid "SSID:"
#~ msgstr "SSID:"

#~ msgid "Save"
#~ msgstr "Save"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "Tobias Fella"
#~ msgstr "Tobias Fella"
